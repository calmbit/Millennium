CC=g++
CFLAGS=--std=c++17 -g -Wall -Wpedantic -Wextra -Isrc/include -I/usr/include/SDL2 -I/usr/include/SOIL -Wno-unknown-pragmas -O0
LFLAGS=-lGL -lGLEW -lSOIL -lSDL2
LD=g++
BINARY=millennium
SRCPRE=src/
OBJPRE=obj/
BINPRE=bin/
DEFFOLDER=def
OBJS=$(patsubst $(SRCPRE)%.cpp,$(OBJPRE)%.o,$(wildcard $(SRCPRE)*.cpp))
MKDIR_P=mkdir
CP_P=cp
RM_P=rm

default: directories all assets

directories: $(OBJPRE) $(BINPRE)

assets:
	$(CP_P) -r $(DEFFOLDER) $(BINPRE)

$(OBJPRE):
	$(MKDIR_P) $(OBJPRE)

$(BINPRE):
	$(MKDIR_P) $(BINPRE)

all: $(BINPRE)$(BINARY)

$(BINPRE)$(BINARY): $(OBJS)
	$(LD) $(OBJS) $(LFLAGS) -o $@



$(OBJPRE)%.o:$(SRCPRE)%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM_P) -rf $(BINPRE)*
	$(RM_P) -rf $(OBJPRE)*.o
