# Millennium Worldbuilding Notes

<br>

----------------

**DISCLAIMER**: All information in this document is subject to be changed at any point in time as some rudimentary structure for the game coalesces.

----------------

<br>

## Magic/Religion

* The four principal powers (elements):
	* Aqua  - Water
	* Ignis - Fire
	* Terra - Earth
	* Caeli - Air

* The four primary planetary bodies:
  
	| Planet     | Aligned | Maligned |
	|:----------:|:-------:|:--------:|
	| **Alaga**  | Ignis   | Aqua     |
	| **Malens** | Aqua    | Ignis    |
	| **Tenser** | Terra   | Caeli    |
	| **Bentka** | Caeli   | Terra    |



 * The four secondary co-aligned bodies (phases at a different rate)

	| Planet     | Co-Aligned | Co-Aligned |
	|:----------:|:----------:|:----------:|
	| **Nanlen** | Aqua       | Terra      |
	| **Buqqo**  | Aqua       | Caeli      |
	| **Vunaz**  | Ignis      | Terra      |
	| **Polnak** | Ignis      | Caeli      |

* Maligned bodies **SHALL NOT** intermingle. Co-aligned bodies have limited connections, but with pentalties.

 * 36 Gods - One for each alignment (Values/Morals/Element, e.g. Chaotic/Good/Terra)
