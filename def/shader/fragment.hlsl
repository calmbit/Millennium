#version 150 core

flat in ivec3 Color;
in vec2 Texcoord;
out vec4 outColor;

uniform sampler2D tex;

uniform vec3 colors[16];

void main()
{
    vec4 texColor = texture(tex, Texcoord);
    vec4 background = vec4(colors[Color[1]], 1.0f);
    vec4 foreground = vec4(colors[Color[0] + (Color[2] == 1 ? 8 : 0)], 1.0f);
    //vec4 background = vec4(0.0,0.0,0.0,1.0);
    //vec4 foreground = vec4(1.0,1.0,1.0, 1.0);
    //vec4 background = vec4(colors[0], 1.0f);
    //vec4 foreground = vec4(colors[1], 1.0f);
    outColor = mix(background, texColor * foreground, texColor.a);
}