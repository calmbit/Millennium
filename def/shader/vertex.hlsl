#version 150 core

in vec2 position;
in ivec3 color;
in vec2 texcoord;

flat out ivec3 Color;
out vec2 Texcoord;

float rand(vec3 co){
    return fract(tan(dot(co.xy ,vec2(12.9898,78.33*co.z))) * 4358.5453);
}

void main()
{
    Color = color;
    Texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}