/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "alignment.hpp"

namespace mlnm {
	Alignment::Alignment(AlignmentValues values,
			     AlignmentMorality morality,
			     AlignmentElement element) {
		this->values = values;
		this->morality = morality;
		this->element = element;
	}

	Alignment::Alignment() {
		this->values = AlignmentValues::NEUTRAL;
		this->morality = AlignmentMorality::NEUTRAL;
		this->element = AlignmentElement::NEUTRUM;
	}

	Alignment::~Alignment() {}
	bool Alignment::hasValues(AlignmentValues values) {
		return this->values == values;
	}
	bool Alignment::hasMorality(AlignmentMorality morality) {
		return this->morality == morality;
	}
	bool Alignment::hasElement(AlignmentElement element) {
		return this->element == element;
	}
}