/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "behaviordrunk.hpp"

#include "entityinstance.hpp"
#include "random.hpp"

namespace mlnm {
	BehaviorDrunk::BehaviorDrunk()
	  : Behavior() {}

	BehaviorDrunk::~BehaviorDrunk() {}

	void BehaviorDrunk::start() {}

	void BehaviorDrunk::tick(EntityInstanceRef ent) {
		int xO = Random::rolld3(1) - 2;
		int yO = Random::rolld3(1) - 2;

		ent->offsetPosition(xO, yO);
	}

	void BehaviorDrunk::end() {}
}