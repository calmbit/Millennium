/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "color.hpp"

#include <iostream>
#include <map>

#include "logger.hpp"

namespace mlnm {
	ColorSet globalColors;

	const char* COLOR_NAMES[COLOR_COUNT] = {
		"BLACK", "BLUE",     "GREEN",  "CYAN",  "RED",    "MAGENTA",
		"BROWN", "LGRAY",    "DGRAY",  "LBLUE", "LGREEN", "LCYAN",
		"LRED",  "LMAGENTA", "YELLOW", "WHITE"
	};

	RGBColor::RGBColor() {
		this->r = 0;
		this->g = 0;
		this->b = 0;
	}

	RGBColor::RGBColor(int r, int g, int b) {
		this->r = r;
		this->g = g;
		this->b = b;
	}

	RGBColor::RGBColor(const RGBColor& rhs) {
		this->r = rhs.r;
		this->g = rhs.g;
		this->b = rhs.b;
	}

	RGBColor::~RGBColor() {}

	IndexedColor::IndexedColor() {
		this->fg = 0;
		this->bg = 0;
		this->bd = 0;
	}

	IndexedColor::IndexedColor(unsigned char fg,
				   unsigned char bg,
				   unsigned char bd) {
		this->fg = fg;
		this->bg = bg;
		this->bd = bd;
	}

	IndexedColor::IndexedColor(const IndexedColor& rhs) {
		this->fg = rhs.fg;
		this->bg = rhs.bg;
		this->bd = rhs.bd;
	}

	IndexedColor::~IndexedColor() {}

	ColorSet::ColorSet() {
		this->good = false;
		this->name = "";
	}

	ColorSet::ColorSet(const ColorSet& rhs) {
		this->good = rhs.good;
		this->name = rhs.name;
		for (auto i = 0; i < static_cast<int>(ColorType::COUNT); i++) {
			this->colors[i] = rhs.colors[i];
		}
	}

	ColorSet::~ColorSet() {}

	void to_json(json& j, const RGBColor& c) {
		j = json{ { "R", c.r }, { "B", c.b }, { "G", c.g } };
	}

	void from_json(const json& j, RGBColor& c) {
		c.r = j.at("R").get<int>();
		c.g = j.at("G").get<int>();
		c.b = j.at("B").get<int>();
	}

	void to_json(json& j, const ColorSet& c) {
		j = json{ { COLOR_NAMES[static_cast<int>(ColorType::BLACK)],
			    c.getColor(ColorType::BLACK) },
			  { COLOR_NAMES[static_cast<int>(ColorType::BLUE)],
			    c.getColor(ColorType::BLUE) },
			  { COLOR_NAMES[static_cast<int>(ColorType::GREEN)],
			    c.getColor(ColorType::GREEN) },
			  { COLOR_NAMES[static_cast<int>(ColorType::CYAN)],
			    c.getColor(ColorType::CYAN) },
			  { COLOR_NAMES[static_cast<int>(ColorType::RED)],
			    c.getColor(ColorType::RED) },
			  { COLOR_NAMES[static_cast<int>(ColorType::MAGENTA)],
			    c.getColor(ColorType::MAGENTA) },
			  { COLOR_NAMES[static_cast<int>(ColorType::BROWN)],
			    c.getColor(ColorType::BROWN) },
			  { COLOR_NAMES[static_cast<int>(ColorType::LGRAY)],
			    c.getColor(ColorType::LGRAY) },
			  { COLOR_NAMES[static_cast<int>(ColorType::DGRAY)],
			    c.getColor(ColorType::DGRAY) },
			  { COLOR_NAMES[static_cast<int>(ColorType::LBLUE)],
			    c.getColor(ColorType::LBLUE) },
			  { COLOR_NAMES[static_cast<int>(ColorType::LGREEN)],
			    c.getColor(ColorType::LGREEN) },
			  { COLOR_NAMES[static_cast<int>(ColorType::LCYAN)],
			    c.getColor(ColorType::LCYAN) },
			  { COLOR_NAMES[static_cast<int>(ColorType::LRED)],
			    c.getColor(ColorType::LRED) },
			  { COLOR_NAMES[static_cast<int>(ColorType::LMAGENTA)],
			    c.getColor(ColorType::LMAGENTA) },
			  { COLOR_NAMES[static_cast<int>(ColorType::YELLOW)],
			    c.getColor(ColorType::YELLOW) },
			  { COLOR_NAMES[static_cast<int>(ColorType::WHITE)],
			    c.getColor(ColorType::WHITE) } };
	}

	void from_json(const json& j, ColorSet& c) {
		c.name = j.at("name").get<std::string>();
		auto colors =
		  j.at("scheme").get<std::map<std::string, RGBColor>>();
		for (auto i = 0; i < COLOR_COUNT; i++) {
			if (colors.find(COLOR_NAMES[i]) == colors.end()) {
				LogEntryBuilder{ LoggerLevel::ERROR, "color" }
				  .addString("Color ")
				  .addString(COLOR_NAMES[i])
				  .addString(" isn't defined in")
				  .addString(c.name)
				  .log();
				c.good = false;
				return;
			}
			c.colors[i] = colors.at(COLOR_NAMES[i]);
		}
		c.good = true;
	}

	float* ColorSet::getColors() {
		float* arr = new float[COLOR_COUNT * COLOR_COMPONENTS];

		for (auto i = 0; i < COLOR_COUNT; i++) {
			arr[(i * 3)] = this->colors[i].r * COLOR_QUOTIENT;
			arr[(i * 3) + 1] = this->colors[i].g * COLOR_QUOTIENT;
			arr[(i * 3) + 2] = this->colors[i].b * COLOR_QUOTIENT;
		}

		return arr;
	}

	unsigned char decomposeIndexedColor(IndexedColor& color) {
		return decomposeComponents(color.fg, color.bg, color.bd);
	}

	IndexedColor composeIndexedColorFromComponents(unsigned char fg,
						       unsigned char bg,
						       unsigned char bd) {
		return IndexedColor{ fg, bg, bd };
	}

	unsigned char decomposeComponents(unsigned char fg,
					  unsigned char bg,
					  unsigned char bd) {
		unsigned char fin = 0;

		fin += ((fg & INDEXED_COLOR_MASK) << FOREGROUND_SHIFT);
		fin += ((bg & INDEXED_COLOR_MASK) << BACKGROUND_SHIFT);
		fin += ((bd & INDEXED_COLOR_MASK) << BOLD_SHIFT);

		return fin;
	}

	IndexedColor composeIndexedColorFromDecomposed(unsigned char color) {
		return IndexedColor{
			static_cast<unsigned char>(
			  ((color & FOREGROUND_MASK) >> FOREGROUND_SHIFT)),
			static_cast<unsigned char>(
			  ((color & BACKGROUND_MASK) >> BACKGROUND_SHIFT)),
			static_cast<unsigned char>(
			  ((color & BOLD_MASK) >> BOLD_SHIFT))
		};
	}
} // namespace mlnm