/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "entity.hpp"

#include <functional>
#include <iostream>

#include "color.hpp"
#include "entityinstance.hpp"
#include "event.hpp"
#include "font.hpp"
#include "logger.hpp"
#include "random.hpp"
#include "window.hpp"

namespace mlnm {

	Entity::Entity() {
		this->tile = '\0';
		this->color = IndexedColor{ 0, 0, 0 };
		this->maxHealth = 0;
		this->entityClass = EntityClass::UNKNOWN;
		this->defaultAlignment = Alignment{};
		this->level = DOES_NOT_SPAWN;
		this->levelOffset = DOES_NOT_SPAWN;
		this->speed = IMMOBILE;
		this->update = [](EntityInstanceRef) {};
		this->tick = [](EntityInstanceRef) {};
		this->draw = [](EntityInstanceRef) {};
	}

	Entity::Entity(unsigned char tile,
		       IndexedColor color,
		       Alignment alignment,
		       unsigned int level,
		       unsigned int levelOffset,
		       unsigned int speed,
		       EntityFuncDelegate update,
		       EntityFuncDelegate tick,
		       EntityFuncDelegate draw)
	  : Entity() {
		this->tile = tile;
		this->color = color;
		this->maxHealth = 0;
		this->entityClass = EntityClass::UNKNOWN;
		this->defaultAlignment = alignment;
		this->level = level;
		this->levelOffset = levelOffset;
		this->speed = speed;
		this->update = update;
		this->tick = tick;
		this->draw = draw;
	}

	Entity::~Entity() {}

	void Entity::callUpdate(EntityInstanceRef ent) { this->update(ent); }

	void Entity::callTick(EntityInstanceRef ent) { this->tick(ent); }

	void Entity::callDraw(EntityInstanceRef ent) { this->draw(ent); }

	std::string Entity::getName() { return this->entityName; }

	unsigned char Entity::getTile() { return this->tile; }

	const IndexedColor& Entity::getColor() { return this->color; }

	unsigned int Entity::getMaxHealth() { return this->maxHealth; }

	EntityClass Entity::getClass() { return this->entityClass; }

	const Alignment& Entity::getDefaultAlignment() {
		return this->defaultAlignment;
	}

} // namespace mlnm