/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "entitydemon.hpp"

#include "behaviordrunk.hpp"
#include "entityinstance.hpp"
#include "random.hpp"

namespace mlnm {

	EntityDemon::EntityDemon(std::string name,
				 IndexedColor color,
				 unsigned int level,
				 unsigned int levelOffset,
				 unsigned int speed,
				 AlignmentElement element)
	  : EntityMonster('&',
			  color,
			  Alignment{ AlignmentValues::CHAOTIC,
				     AlignmentMorality::EVIL,
				     element },
			  level,
			  levelOffset,
			  speed) {
		this->entityClass = EntityClass::DEMON;
		this->entityName = name;
	}

	EntityDemon::~EntityDemon() {}

	void EntityDemon::callTick(EntityInstanceRef ent) {
		/*int xO = Random::rolld3(1) - 2;
		int yO = Random::rolld3(1) - 2;

		ent->offsetPosition(xO, yO);*/
	}

	void EntityDemon::fillBehaviors(EntityInstanceRef ent) {
		BehaviorRef r = std::static_pointer_cast<Behavior>(
		  std::make_shared<BehaviorDrunk>());
		ent->addBehavior(r);
	}
}
