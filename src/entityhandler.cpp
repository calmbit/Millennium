/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "entityhandler.hpp"

#include "entitydemon.hpp"
#include "entityplayer.hpp"
#include "entityplayerinstance.hpp"
#include "localization.hpp"

namespace mlnm {
	EntityList EntityHandler::entityList;
	EntityDict EntityHandler::entityDefinitions;

	void EntityHandler::init() {
		entityDefinitions = EntityDict{};
		entityList = EntityList{};

		registerEntity("player", new EntityPlayer{});
		registerEntity(
		  getLocaleToken("ent_water_demon"),
		  new EntityDemon{ getLocaleToken("ent_water_demon"),
				   IndexedColor{ 1, 0, 0 },
				   6,
				   4,
				   12,
				   AlignmentElement::AQUA });
		registerEntity(
		  getLocaleToken("ent_earth_demon"),
		  new EntityDemon{ getLocaleToken("ent_earth_demon"),
				   IndexedColor{ 2, 0, 0 },
				   6,
				   4,
				   12,
				   AlignmentElement::TERRA });
		registerEntity(
		  getLocaleToken("ent_fire_demon"),
		  new EntityDemon{ getLocaleToken("ent_fire_demon"),
				   IndexedColor{ 4, 0, 0 },
				   6,
				   4,
				   12,
				   AlignmentElement::IGNIS });
		registerEntity(getLocaleToken("ent_air_demon"),
			       new EntityDemon{ getLocaleToken("ent_air_demon"),
						IndexedColor{ 6, 0, 0 },
						6,
						4,
						12,
						AlignmentElement::CAELI });
	}

	void EntityHandler::registerEntity(std::string name, Entity* ent) {
		entityDefinitions.insert(
		  std::make_pair(name, EntityDefinition{ std::move(ent) }));
	}

	EntityInstanceRef EntityHandler::createEntity(std::string name) {

		if (entityDefinitions.find(name) != entityDefinitions.end()) {
			auto def = entityDefinitions.at(name);

			entityList.push_back(
			  std::make_shared<EntityInstance>(def));

			entityList.back()->fillBehaviors();
			return entityList.back()->getSharedRef();
		} else {
			return nullptr;
		}
	}

	EntityPlayerInstanceRef EntityHandler::createPlayer(
	  IndexedColor color,
	  Alignment alignment) {
		entityList.push_back(std::make_shared<EntityPlayerInstance>(
		  entityDefinitions.at("player"), color, alignment));

		entityList.back()->fillBehaviors();
		entityList.back()->setPosition(2, 2);

		return std::dynamic_pointer_cast<EntityPlayerInstance>(
		  entityList.back()->getSharedRef());
	}

	void EntityHandler::destroy() {
		entityDefinitions.clear();
		entityList.clear();
	}

	void EntityHandler::draw() {
		for (auto f : entityList) {
			f->draw();
		}
	}

	void EntityHandler::update() {
		for (auto f : entityList) {
			f->update();
		}
	}

	void EntityHandler::tick() {
		for (auto f : entityList) {
			f->tick();
		}
	}
}
