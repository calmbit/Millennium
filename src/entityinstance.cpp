/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "entityinstance.hpp"
#include "behavior.hpp"
#include <iostream>

namespace mlnm {

	EntityInstance::EntityInstance() {
		this->entity = nullptr;
		this->tile = '\0';
		this->color = IndexedColor{};
		this->health = 0;
		this->currentAlignment = Alignment{};
		this->x = NOT_IN_WORLD;
		this->y = NOT_IN_WORLD;
		this->behaviorList = std::vector<BehaviorRef>();
	}

	EntityInstance::EntityInstance(EntityDefinition def) {
		this->entity = def;
		this->tile = def->getTile();
		this->color = def->getColor();
		this->health = def->getMaxHealth();
		this->currentAlignment = def->getDefaultAlignment();
		this->x = NOT_IN_WORLD;
		this->y = NOT_IN_WORLD;
		this->behaviorList = std::vector<BehaviorRef>();
	}

	EntityInstance::~EntityInstance() {}

	std::shared_ptr<EntityInstance> EntityInstance::getSharedRef() {
		return shared_from_this();
	}

	void EntityInstance::draw() {
		if (this->x == NOT_IN_WORLD || this->y == NOT_IN_WORLD)
			return;
		this->entity->callDraw(shared_from_this());
	}

	void EntityInstance::update() {
		if (this->x == NOT_IN_WORLD || this->y == NOT_IN_WORLD)
			return;
		this->entity->callUpdate(shared_from_this());
	}

	void EntityInstance::tick() {
		if (this->x == NOT_IN_WORLD || this->y == NOT_IN_WORLD)
			return;
		// this->entity->callTick(shared_from_this());

		for (BehaviorRef b : this->behaviorList) {
			b->tick(getSharedRef());
		}
	}

	void EntityInstance::setPosition(int x, int y) {
		this->x = (x < 1 ? 1 : (x >= ROWS_X - 1 ? ROWS_X - 2 : x));
		this->y = (y < 1 ? 1 : (y >= ROWS_Y - 1 ? ROWS_Y - 2 : y));
	}

	void EntityInstance::offsetPosition(int x, int y) {
		this->setPosition(this->x + x, this->y + y);
	}

	bool EntityInstance::isDead() { return this->health <= 0; }

	bool EntityInstance::isClass(EntityClass compClass) {
		return this->entity->getClass() == compClass;
	}

	int EntityInstance::getX() { return this->x; }

	int EntityInstance::getY() { return this->y; }

	unsigned char EntityInstance::getTile() { return this->tile; }

	const IndexedColor& EntityInstance::getColor() { return this->color; }

	void EntityInstance::setAlignment(Alignment newAlignment) {
		this->currentAlignment = newAlignment;
	}

	void EntityInstance::damage(int value) {
		this->health =
		  (this->health - value <= 0
		     ? 0
		     : (this->health - value > this->entity->getMaxHealth()
			  ? this->entity->getMaxHealth()
			  : this->health - value));
	}

	bool EntityInstance::isPlayer() { return false; }

	void EntityInstance::fillBehaviors() {
		if (entity != nullptr)
			entity->fillBehaviors(getSharedRef());
	}

	void EntityInstance::addBehavior(BehaviorRef behavior) {
		this->behaviorList.push_back(behavior);
	}
}