/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "entitymonster.hpp"

#include "entity.hpp"
#include "entityinstance.hpp"
#include "font.hpp"
#include "random.hpp"

namespace mlnm {

	EntityMonster::EntityMonster(unsigned char tile,
				     IndexedColor color,
				     Alignment alignment,
				     unsigned int level,
				     unsigned int levelOffset,
				     unsigned int speed)
	  : Entity(tile, color, alignment, level, levelOffset, speed) {}

	void EntityMonster::callTick(EntityInstanceRef ent) {}

	void EntityMonster::callDraw(EntityInstanceRef ent) {
		FontManager::addTileToRender(ent->getTile(),
					     ent->getX(),
					     ent->getY(),
					     ent->getColor().fg,
					     ent->getColor().bg,
					     ent->getColor().bd);
	}

	void EntityMonster::fillBehaviors(EntityInstanceRef ent) {}
}
