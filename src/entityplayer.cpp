/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
#include "entityplayer.hpp"

#include "alignment.hpp"
#include "color.hpp"

namespace mlnm {

	EntityPlayer::EntityPlayer()
	  : Entity('@',
		   IndexedColor{},
		   Alignment{},
		   DOES_NOT_SPAWN,
		   DOES_NOT_SPAWN,
		   IMMOBILE) {}

	EntityPlayer::~EntityPlayer() {}

	void EntityPlayer::fillBehaviors(EntityInstanceRef ent) {}
}