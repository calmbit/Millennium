/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "entityplayerinstance.hpp"

#include "window.hpp"

namespace mlnm {

	EntityPlayerInstance::EntityPlayerInstance(EntityDefinition def,
						   IndexedColor color,
						   Alignment alignment)
	  : EntityInstance(def) {
		this->wD = this->sD = this->aD = this->dD = false;
		this->wT = this->sT = this->aT = this->dT = 0;

		this->color = color;
		this->currentAlignment = alignment;

		Window::bindCallback(SDL_EventType::SDL_KEYDOWN,
				     std::bind(&EntityPlayerInstance::onKeyDown,
					       this,
					       std::placeholders::_1));
		Window::bindCallback(SDL_EventType::SDL_KEYUP,
				     std::bind(&EntityPlayerInstance::onKeyUp,
					       this,
					       std::placeholders::_1));
	}

	void EntityPlayerInstance::update() {
		if (wD && wT > 0) {
			wT--;
		} else if (wD) {
			setPosition(x, y - 1);
			EventBus::fireEvent(
			  "WORLD",
			  new Event{ EventType::WORLD_TICK, nullptr, nullptr });
			wT = 2;
		}
		if (sD && sT > 0) {
			sT--;
		} else if (sD) {
			setPosition(x, y + 1);
			EventBus::fireEvent(
			  "WORLD",
			  new Event{ EventType::WORLD_TICK, nullptr, nullptr });
			sT = 2;
		}
		if (aD && aT > 0) {
			aT--;
		} else if (aD) {
			setPosition(x - 1, y);
			EventBus::fireEvent(
			  "WORLD",
			  new Event{ EventType::WORLD_TICK, nullptr, nullptr });
			aT = 2;
		}
		if (dD && dT > 0) {
			dT--;
		} else if (dD) {
			setPosition(x + 1, y);
			EventBus::fireEvent(
			  "WORLD",
			  new Event{ EventType::WORLD_TICK, nullptr, nullptr });
			dT = 2;
		}
	}

	void EntityPlayerInstance::draw() {

		FontManager::addTileToRender(this->tile,
					     this->x,
					     this->y,
					     this->color.fg,
					     this->color.bg,
					     this->color.bd);

		/*if (!name.empty()) {
			FontManager::renderText(name.c_str(), 40 - (name.size()
		/ 2), 0, 2, 0, 1);
		}*/
	}

	void EntityPlayerInstance::onKeyDown(SDL_Event* evt) {
		// TODO: This logic needs to be made _much_ more pretty.
		switch (evt->key.keysym.sym) {
			case static_cast<int>(SDLK_w):
			case static_cast<int>(SDLK_UP): {
				if (!wD) {
					setPosition(x, y - 1);
					wD = true;
					wT = 45;
					EventBus::fireEvent(
					  "WORLD",
					  new Event{ EventType::WORLD_TICK,
						     nullptr,
						     nullptr });
				}
				break;
			}
			case static_cast<int>(SDLK_s):
			case static_cast<int>(SDLK_DOWN): {
				if (!sD) {
					setPosition(x, y + 1);
					sD = true;
					sT = 45;
					EventBus::fireEvent(
					  "WORLD",
					  new Event{ EventType::WORLD_TICK,
						     nullptr,
						     nullptr });
				}
				break;
			}
			case static_cast<int>(SDLK_a):
			case static_cast<int>(SDLK_LEFT): {
				if (!aD) {
					setPosition(x - 1, y);
					aD = true;
					aT = 45;
					EventBus::fireEvent(
					  "WORLD",
					  new Event{ EventType::WORLD_TICK,
						     nullptr,
						     nullptr });
				}
				break;
			}
			case static_cast<int>(SDLK_d):
			case static_cast<int>(SDLK_RIGHT): {
				if (!dD) {
					setPosition(x + 1, y);
					dD = true;
					dT = 45;
					EventBus::fireEvent(
					  "WORLD",
					  new Event{ EventType::WORLD_TICK,
						     nullptr,
						     nullptr });
				}
				break;
			}
		}
	}

	void EntityPlayerInstance::onKeyUp(SDL_Event* evt) {
		switch (evt->key.keysym.sym) {
			case static_cast<int>(SDLK_w):
			case static_cast<int>(SDLK_UP): {
				wD = false;
				break;
			}
			case static_cast<int>(SDLK_s):
			case static_cast<int>(SDLK_DOWN): {
				sD = false;
				break;
			}
			case static_cast<int>(SDLK_a):
			case static_cast<int>(SDLK_LEFT): {
				aD = false;
				break;
			}
			case static_cast<int>(SDLK_d):
			case static_cast<int>(SDLK_RIGHT): {
				dD = false;
				break;
			}
		}
	}

	bool EntityPlayerInstance::isPlayer() { return true; }
}