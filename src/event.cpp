/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "event.hpp"

namespace mlnm {

	Event::Event(EventType type, Entity* src, Entity* dst) {
		this->type = type;
		this->src = src;
		this->dst = dst;
	}

	EventType Event::getType() { return this->type; }
	Entity* Event::getSrc() { return this->src; }
	Entity* Event::getDest() { return this->dst; }

	EventQueueDict EventBus::buses;

	void EventBus::init() {
		buses = EventQueueDict{};
		createBus("WORLD");
	}

	void EventBus::createBus(std::string name) {
		buses.insert(std::make_pair(name, EventQueue{}));
	}

	void EventBus::fireEvent(std::string bus, Event* event) {
		if (buses.find(bus) != buses.end()) {
			buses.at(bus).push(EventRef{ std::move(event) });
		}
	}

	EventRef EventBus::pollBus(std::string bus) {
		if (buses.find(bus) != buses.end() && !buses.at(bus).empty()) {
			auto evt = buses.at(bus).front();
			buses.at(bus).pop();
			return evt;
		} else
			return nullptr;
	}
}