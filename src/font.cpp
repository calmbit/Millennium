/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "font.hpp"

#include <iostream>

namespace mlnm {
	unsigned char FontManager::screen[ROWS_X][ROWS_Y];
	unsigned char FontManager::lastScreen[ROWS_X][ROWS_Y];
	unsigned char FontManager::colors[ROWS_X][ROWS_Y];
	unsigned char FontManager::lastColors[ROWS_X][ROWS_Y];
	float FontManager::textureVertTable[FONT_TILES_X][FONT_TILES_Y]
					   [COORDS_PER_TILE];

	void FontManager::init() {
		for (auto y = 0; y < FONT_TILES_Y; y++) {
			for (auto x = 0; x < FONT_TILES_X; x++) {
				textureVertTable[x][y][0] =
				  x * TEXCOORD_QUOTIENT_X;
				textureVertTable[x][y][1] =
				  y * TEXCOORD_QUOTIENT_Y;

				textureVertTable[x][y][2] =
				  (x + 1) * TEXCOORD_QUOTIENT_X;
				textureVertTable[x][y][3] =
				  y * TEXCOORD_QUOTIENT_Y;

				textureVertTable[x][y][4] =
				  (x + 1) * TEXCOORD_QUOTIENT_X;
				textureVertTable[x][y][5] =
				  (y + 1) * TEXCOORD_QUOTIENT_Y;

				textureVertTable[x][y][6] =
				  (x + 1) * TEXCOORD_QUOTIENT_X;
				textureVertTable[x][y][7] =
				  (y + 1) * TEXCOORD_QUOTIENT_Y;

				textureVertTable[x][y][8] =
				  x * TEXCOORD_QUOTIENT_X;
				textureVertTable[x][y][9] =
				  (y + 1) * TEXCOORD_QUOTIENT_Y;

				textureVertTable[x][y][10] =
				  x * TEXCOORD_QUOTIENT_X;
				textureVertTable[x][y][11] =
				  y * TEXCOORD_QUOTIENT_Y;
			}
		}
		clearScreen();
	}

	void FontManager::buildScreen(int* otherColors, float* texcoords) {
		for (auto y = 0; y < ROWS_Y; y++) {
			for (auto x = 0; x < ROWS_X; x++) {
				if (colors[x][y] != lastColors[x][y]) {
					for (auto i = 0; i < VERTS_PER_TILE;
					     i++) {
						otherColors
						  [(y * ROWS_X *
						    COLOR_VERTS_PER_TILE) +
						   (x * COLOR_VERTS_PER_TILE) +
						   (i * COLOR_COMPONENTS)] =
						    ((colors[x][y] &
						      FOREGROUND_MASK) >>
						     FOREGROUND_SHIFT);
						otherColors
						  [(y * ROWS_X *
						    COLOR_VERTS_PER_TILE) +
						   (x * COLOR_VERTS_PER_TILE) +
						   (i * COLOR_COMPONENTS) + 1] =
						    ((colors[x][y] &
						      BACKGROUND_MASK) >>
						     BACKGROUND_SHIFT);
						otherColors
						  [(y * ROWS_X *
						    COLOR_VERTS_PER_TILE) +
						   (x * COLOR_VERTS_PER_TILE) +
						   (i * COLOR_COMPONENTS) + 2] =
						    ((colors[x][y] &
						      BOLD_MASK) >>
						     BOLD_SHIFT);
					}
				}
				if (screen[x][y] != lastScreen[x][y]) {
					fillTexcoord(
					  texcoords +
					    (y * ROWS_X * COORDS_PER_TILE) +
					    (x * COORDS_PER_TILE),
					  (screen[x][y] % FONT_TILES_X),
					  (screen[x][y] / FONT_TILES_Y));
				}
			}
		}
	}

	void FontManager::fillTexcoord(float* texcoords, int x, int y) {
		// The thought here is if we're already rendering 'nothing', why
		// not just render nothing?
		if (x == 0 && y == 0)
			return;

		for (auto i = 0; i < COORDS_PER_TILE; i++) {
			texcoords[i] = textureVertTable[x][y][i];
		}
	}

	void FontManager::clearScreen() {
		for (auto y = 0; y < ROWS_Y; y++) {
			for (auto x = 0; x < ROWS_X; x++) {
				lastScreen[x][y] = screen[x][y];
				lastColors[x][y] = colors[x][y];
				screen[x][y] = 0;
				colors[x][y] = 0;
			}
		}
	}

	void FontManager::addTileToRender(unsigned char tile,
					  int x,
					  int y,
					  int fg,
					  int bg,
					  int bd) {
		screen[x][y] = tile;
		colors[x][y] = decomposeComponents(fg, bg, bd);
	}

	void FontManager::renderText(const char* str,
				     int x,
				     int y,
				     int fg,
				     int bg,
				     int bd) {
		for (auto i = 0; str[i] != '\0'; i++) {
			addTileToRender(str[i], x + i, y, fg, bg, bd);
		}
	}

	void FontManager::renderInt(int val,
				    int x,
				    int y,
				    int fg,
				    int bg,
				    int bd) {
		renderText(std::to_string(val).c_str(), x, y, fg, bg, bd);
	}

	void FontManager::renderBorder(int fg, int bg, int bd) {
		for (auto y = 0; y < ROWS_Y; y++) {
			for (auto x = 0; x < ROWS_X; x++) {
				if (y == 0 || y == ROWS_Y - 1 || x == 0 ||
				    x == ROWS_X - 1) {
					addTileToRender(' ', x, y, fg, bg, bd);
				}
			}
		}
	}

	void FontManager::renderFontTest(int x, int y) {
		for (auto yt = 0; yt < FONT_TILES_Y; yt++) {
			for (auto xt = 0; xt < FONT_TILES_X; xt++) {
				addTileToRender((yt * FONT_TILES_X) + xt + x,
						xt + x,
						yt + y,
						yt % 7,
						0,
						((yt % 7) != 0 ? xt % 2 : 1));
			}
		}
	}
} // namespace mlnm