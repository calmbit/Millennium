/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ALIGNMENT_HPP
#define MLNM_ALIGNMENT_HPP

namespace mlnm {

	/**
	 * Q: Why is this a seperate header?
	 * A: Alignment exists in a few more places than could be accurately
	 * represented in one header file without a lot of baggage, so we're
	 * gonna skip that and create a seperate header file and struct to
	 * manage this information independently.
	 */

	enum class AlignmentMorality : unsigned char { GOOD, NEUTRAL, EVIL };
	enum class AlignmentValues : unsigned char { LAWFUL, NEUTRAL, CHAOTIC };

	enum class AlignmentElement : unsigned char {
		NEUTRUM,
		AQUA,
		IGNIS,
		TERRA,
		CAELI,
		OMNIS
	};

	struct Alignment {
		AlignmentMorality morality;
		AlignmentValues values;
		AlignmentElement element;
		Alignment();
		Alignment(AlignmentValues values,
			  AlignmentMorality morality,
			  AlignmentElement element = AlignmentElement::NEUTRUM);
		~Alignment();
		bool hasValues(AlignmentValues values);
		bool hasMorality(AlignmentMorality morality);
		bool hasElement(AlignmentElement element);
	};
}

#endif
