/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_BEHAVIORDRUNK_HPP
#define MLNM_BEHAVIORDRUNK_HPP

#include "behavior.hpp"

namespace mlnm {
	class BehaviorDrunk : public Behavior {
	public:
		BehaviorDrunk();
		~BehaviorDrunk();
		void start();
		void tick(EntityInstanceRef ent);
		void end();
	};
}
#endif