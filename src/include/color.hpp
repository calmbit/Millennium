/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_COLOR_HPP
#define MLNM_COLOR_HPP

#include <string>

#include "json.hpp"

using nlohmann::json;

namespace mlnm {
	enum class ColorType {
		BLACK,
		BLUE,
		GREEN,
		CYAN,
		RED,
		MAGENTA,
		BROWN,
		LGRAY,
		DGRAY,
		LBLUE,
		LGREEN,
		LCYAN,
		LRED,
		LMAGENTA,
		YELLOW,
		WHITE,
		COUNT
	};

	const int COLOR_COUNT = static_cast<int>(ColorType::COUNT);
	const int COLOR_COMPONENTS = 3;
	const int MAXIMUM_COLOR_VALUE = 255;
	const float COLOR_QUOTIENT =
	  1.0f / static_cast<float>(MAXIMUM_COLOR_VALUE);
	const unsigned char INDEXED_COLOR_MASK = 0x7;
	const unsigned char FOREGROUND_MASK = 0x7;
	const unsigned char FOREGROUND_SHIFT = 0;
	const unsigned char BACKGROUND_MASK = 0x38;
	const unsigned char BACKGROUND_SHIFT = 3;
	const unsigned char BOLD_MASK = 0x40;
	const unsigned char BOLD_SHIFT = 6;
	extern const char* COLOR_NAMES[COLOR_COUNT];

	struct RGBColor {
		int r, g, b;
		RGBColor();
		RGBColor(int r, int g, int b);
		RGBColor(const RGBColor& rhs);
		~RGBColor();
	};

	struct IndexedColor {
		unsigned char fg, bg, bd;
		IndexedColor();
		IndexedColor(unsigned char fg,
			     unsigned char bg,
			     unsigned char bd);
		IndexedColor(const IndexedColor& rhs);
		~IndexedColor();
	};

	struct ColorSet {
		bool good;
		std::string name;
		RGBColor colors[static_cast<int>(ColorType::COUNT)];
		RGBColor getColor(ColorType type) const {
			return colors[static_cast<int>(type)];
		}
		float* getColors();
		ColorSet();
		ColorSet(const ColorSet& rhs);
		~ColorSet();
	};

	void to_json(json& j, const RGBColor& c);
	void from_json(const json& j, RGBColor& c);
	void to_json(json& j, const ColorSet& c);
	void from_json(const json& j, ColorSet& c);

	unsigned char decomposeIndexedColor(IndexedColor& color);
	unsigned char decomposeComponents(unsigned char fg,
					  unsigned char bg,
					  unsigned char bd);
	IndexedColor composeIndexedColorFromComponents(unsigned char fg,
						       unsigned char bg,
						       unsigned char bd);
	IndexedColor composeIndexedColorFromDecomposed(unsigned char color);

	extern ColorSet globalColors;
} // namespace mlnm

#endif
