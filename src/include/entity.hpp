/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ENTITY_HPP
#define MLNM_ENTITY_HPP

#include <functional>
#include <memory>

#include "alignment.hpp"
#include "color.hpp"
#include "font.hpp"

#include <SDL.h>

namespace mlnm {

	const unsigned int DOES_NOT_SPAWN = 0;
	const unsigned int IMMOBILE = 0;
	enum class EntityClass { UNKNOWN, HUMAN, DEMON };

	class Entity;
	class EntityInstance;
	class Behavior;

#pragma region USING_STATEMENTS

	using EntityDefinition = std::shared_ptr<Entity>;
	using EntityDict = std::map<std::string, EntityDefinition>;
	using EntityInstanceRef = std::shared_ptr<EntityInstance>;
	using EntityFuncDelegate = std::function<void(EntityInstanceRef)>;

#pragma endregion USING_STATEMENTS

	class Entity {
	protected:
		std::string entityName;
		unsigned char tile;
		unsigned int level;
		unsigned int levelOffset;
		unsigned int speed;
		IndexedColor color;
		unsigned int maxHealth;
		EntityClass entityClass;
		Alignment defaultAlignment;
		EntityFuncDelegate update;
		EntityFuncDelegate tick;
		EntityFuncDelegate draw;

	public:
		Entity();
		Entity(unsigned char tile,
		       IndexedColor color,
		       Alignment alignment,
		       unsigned int level,
		       unsigned int levelOffset,
		       unsigned int speed,
		       EntityFuncDelegate update = [](EntityInstanceRef) {},
		       EntityFuncDelegate tick = [](EntityInstanceRef) {},
		       EntityFuncDelegate draw = [](EntityInstanceRef) {});
		virtual ~Entity();
		virtual void fillBehaviors(EntityInstanceRef ent) = 0;
		virtual void callUpdate(EntityInstanceRef ent);
		virtual void callTick(EntityInstanceRef ent);
		virtual void callDraw(EntityInstanceRef ent);
		std::string getName();
		unsigned char getTile();
		const IndexedColor& getColor();
		unsigned int getMaxHealth();
		EntityClass getClass();
		const Alignment& getDefaultAlignment();
	};

} // namespace mlnm

#endif