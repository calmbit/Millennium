/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ENTITYDEMON_HPP
#define MLNM_ENTITYDEMON_HPP

#include <string>

#include "color.hpp"
#include "entitymonster.hpp"

namespace mlnm {

	class EntityDemon : public EntityMonster {

	public:
		EntityDemon(
		  std::string name,
		  IndexedColor color,
		  unsigned int level,
		  unsigned int levelOffset,
		  unsigned int speed,
		  AlignmentElement element = AlignmentElement::NEUTRUM);
		virtual ~EntityDemon();
		void callTick(EntityInstanceRef ent);
		void fillBehaviors(EntityInstanceRef ent);
	};
}

#endif
