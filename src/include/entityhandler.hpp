/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ENTITYHANDLER_HPP
#define MLNM_ENTITYHANDLER_HPP

#include <memory>
#include <vector>

#include "alignment.hpp"
#include "color.hpp"
#include "entity.hpp"
#include "entityinstance.hpp"
#include "entityplayerinstance.hpp"

namespace mlnm {
#pragma region USING_STATEMENTS

	using EntityList = std::vector<EntityInstanceRef>;
	using EntityPlayerInstanceRef = std::shared_ptr<EntityPlayerInstance>;

#pragma endregion USING_STATEMENTS

	class EntityHandler {
	private:
		static EntityDict entityDefinitions;
		static EntityList entityList;

	public:
		static void init();
		static void registerEntity(std::string name, Entity* entity);
		static EntityInstanceRef createEntity(std::string name);
		static EntityPlayerInstanceRef createPlayer(
		  IndexedColor color,
		  Alignment alignment);
		static void destroy();
		static void draw();
		static void update();
		static void tick();
	};
}

#endif
