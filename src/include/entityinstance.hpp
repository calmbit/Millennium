/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ENTITY_INSTANCE
#define MLNM_ENTITY_INSTANCE

#include <vector>

#include "alignment.hpp"
#include "behavior.hpp"
#include "color.hpp"
#include "entity.hpp"

namespace mlnm {

	const int NOT_IN_WORLD = -1;

#pragma region USING_STATEMENTS

	using BehaviorRef = std::shared_ptr<Behavior>;

#pragma endregion USING_STATMENTS

	class EntityInstance
	  : public std::enable_shared_from_this<EntityInstance> {
	protected:
		unsigned char tile;
		IndexedColor color;
		int x, y;
		unsigned int health;
		Alignment currentAlignment;
		EntityDefinition entity;
		std::vector<BehaviorRef> behaviorList;

	public:
		EntityInstance();
		EntityInstance(EntityDefinition def);
		~EntityInstance();
		std::shared_ptr<EntityInstance> getSharedRef();
		virtual void draw();
		virtual void tick();
		virtual void update();
		virtual void setAlignment(Alignment newAlignment);
		virtual void damage(int dmg);
		virtual bool isDead();
		virtual void setPosition(int x, int y);
		virtual void offsetPosition(int x, int y);
		virtual bool isClass(EntityClass compClass);
		virtual bool isPlayer();
		virtual unsigned char getTile();
		virtual const IndexedColor& getColor();
		void fillBehaviors();
		int getX();
		int getY();
		void addBehavior(BehaviorRef behavior);
	};
}

#endif
