/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ENTITYMONSTER_HPP
#define MLNM_ENTITYMONSTER_HPP

#include "alignment.hpp"
#include "color.hpp"
#include "entity.hpp"

namespace mlnm {
	class EntityMonster : public Entity {
	public:
		EntityMonster(unsigned char tile,
			      IndexedColor color,
			      Alignment alignment,
			      unsigned int level,
			      unsigned int levelOffset,
			      unsigned int speed);
		virtual void fillBehaviors(EntityInstanceRef ent);
		virtual void callTick(EntityInstanceRef ent);
		virtual void callDraw(EntityInstanceRef ent);
	};
}

#endif
