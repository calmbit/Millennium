/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ENTITYPLAYER_HPP
#define MLNM_ENTITYPLAYER_HPP

#include "entity.hpp"

namespace mlnm {
	class EntityPlayer : public Entity {
	public:
		EntityPlayer();
		virtual ~EntityPlayer();
		void fillBehaviors(EntityInstanceRef ent);
	};
}

#endif
