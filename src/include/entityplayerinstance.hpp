/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ENTITYPLAYERINSTANCE_HPP
#define MLNM_ENTITYPLAYERINSTANCE_HPP

#include <SDL.h>

#include "entityinstance.hpp"
#include "event.hpp"
#include "font.hpp"

namespace mlnm {

	class EntityPlayerInstance : public EntityInstance {
	private:
		bool wD, sD, aD, dD;
		int wT, sT, aT, dT;

	public:
		EntityPlayerInstance(EntityDefinition def,
				     IndexedColor color,
				     Alignment alignment);
		void onKeyDown(SDL_Event* evt);
		void onKeyUp(SDL_Event* evt);
		void draw();
		void update();
		bool isPlayer();
	};
}

#endif
