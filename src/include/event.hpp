/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_EVENT_HPP
#define MLNM_EVENT_HPP

#include <map>
#include <queue>

#include "entity.hpp"

namespace mlnm {
	enum class EventType { WORLD_TICK, ENTITY_ATTACK };

	class Event;

#pragma region USING_STATEMENTS

	using EventRef = std::shared_ptr<Event>;
	using EventQueue = std::queue<EventRef>;
	using EventQueueDict = std::map<std::string, EventQueue>;

#pragma endregion USING_STATEMENTS

	class Event {
	private:
		EventType type;
		Entity* src;
		Entity* dst;

	public:
		Event(EventType type,
		      Entity* src = nullptr,
		      Entity* dst = nullptr);
		EventType getType();
		Entity* getSrc();
		Entity* getDest();
	};

	class EventBus {
	private:
		static EventQueueDict buses;

	public:
		static void init();
		static void createBus(std::string name);
		static void fireEvent(std::string bus, Event* event);
		static EventRef pollBus(std::string bus);
	};
}
#endif