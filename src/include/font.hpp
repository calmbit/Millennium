/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_FONT_HPP
#define MLNM_FONT_HPP

#include <random>

#include "color.hpp"

namespace mlnm {
	const int ROWS_X = 80;
	const int ROWS_Y = 25;
	const int VERTS_PER_TILE = 6;
	const int COORDS_PER_TILE = VERTS_PER_TILE * 2;
	const int SCREEN_TEXCOORDS_ARRAY_SIZE =
	  ROWS_X * ROWS_Y * COORDS_PER_TILE;
	const int FONT_TILES_X = 16;
	const int FONT_TILES_Y = 16;
	const float TEXCOORD_QUOTIENT_X =
	  1.0f / static_cast<float>(FONT_TILES_X);
	const float TEXCOORD_QUOTIENT_Y =
	  1.0f / static_cast<float>(FONT_TILES_Y);
	const float VERT_INCREMENT_X = 2.0f / static_cast<float>(ROWS_X);
	const float VERT_INCREMENT_Y = -2.0f / static_cast<float>(ROWS_Y);
	const int COLOR_VERTS_PER_TILE = VERTS_PER_TILE * COLOR_COMPONENTS;

	class FontManager {
	private:
		static unsigned char screen[ROWS_X][ROWS_Y];
		static unsigned char lastScreen[ROWS_X][ROWS_Y];
		static unsigned char colors[ROWS_X][ROWS_Y];
		static unsigned char lastColors[ROWS_X][ROWS_Y];
		static float textureVertTable[FONT_TILES_X][FONT_TILES_Y]
					     [COORDS_PER_TILE];

	public:
		static void init();
		static void buildScreen(int* colors, float* texcoords);
		static void addTileToRender(unsigned char tile,
					    int x,
					    int y,
					    int fg,
					    int bg,
					    int bd);
		static void renderText(const char* str,
				       int x,
				       int y,
				       int fg,
				       int bg,
				       int bd);
		static void renderInt(int val,
				      int x,
				      int y,
				      int fg,
				      int bg,
				      int bd);
		static void renderFontTest(int x, int y);
		static void renderBorder(int fg, int bg, int bd);
		static void fillTexcoord(float* texcoords, int x, int y);
		static void clearScreen();
	};
} // namespace mlnm

#endif