/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ITEM_HPP
#define MLNM_ITEM_HPP

#include <map>
#include <memory>
#include <queue>
#include <string>
#include <vector>

#include "alignment.hpp"
#include "color.hpp"
#include "entityinstance.hpp"

namespace mlnm {

	extern const std::string STANDARD_PLURAL;
	extern const std::string ALREADY_PLURAL;

	class Item;
	class ItemStack;

	enum class ItemType {
		UNKNOWN,
		FOOD,
		POTION,
		WAND,
		SCROLL,
		SPELLBOOK,
		RING,
		WEAPON,
		ARMOR
	};

#pragma region USING_STATEMENTS

	using ItemDefinition = std::shared_ptr<Item>;
	using ItemDefinitionDict = std::map<std::string, ItemDefinition>;
	using ItemStackRef = std::shared_ptr<ItemStack>;

	using ItemUseDelegate =
	  std::function<bool(EntityInstanceRef, ItemStackRef)>;

#pragma endregion USING_STATEMENTS

	class Item {
	private:
		std::string name, plural;
		ItemType type;
		unsigned char tile;
		IndexedColor color;
		ItemUseDelegate useDelegate;
		AlignmentElement element; // affects whether or not a
					  // user/monster can utilize the item
					  // w/out penalties

	public:
		Item();
		Item(std::string name,
		     std::string plural,
		     ItemType type,
		     unsigned char tile,
		     IndexedColor color,
		     ItemUseDelegate useDelegate,
		     AlignmentElement element = AlignmentElement::NEUTRUM);
		virtual ~Item();
		virtual std::string getName();
		virtual std::string getPlural();
		virtual unsigned char getTile();
		virtual IndexedColor& getColor();
		bool onUse(EntityInstanceRef ent, ItemStackRef stack);
	};
}

#endif