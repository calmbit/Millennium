/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ITEMFOOD_HPP
#define MLNM_ITEMFOOD_HPP

#include <functional>
#include <memory>

#include "color.hpp"
#include "entity.hpp"
#include "item.hpp"

namespace mlnm {

	class ItemFood;

#pragma region USING_STATEMENTS

	using ItemFoodDefinition = std::shared_ptr<ItemFood>;

#pragma endregion USING_STATEMENTS

	class ItemFood : public Item {
	public:
		enum class SubClass { APPLE, RATION };

		ItemFood(std::string name,
			 std::string plural,
			 IndexedColor color,
			 int foodValue,
			 ItemFood::SubClass subClass);
		~ItemFood();
		bool eat(EntityInstanceRef, ItemStackRef stack);

	private:
		ItemFood::SubClass subClass;
		int foodValue;
	};
}

#endif