/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ITEMHANDLER_HPP
#define MLNM_ITEMHANDLER_HPP

#include <map>
#include <memory>
#include <queue>
#include <vector>

#include "item.hpp"
#include "itemstack.hpp"

namespace mlnm {

	const int RING_VARIATION_COUNT = 12;
	const int BOOK_VARIATION_COUNT = 12;

	extern const std::string RING_VARIATIONS[RING_VARIATION_COUNT];
	extern const std::string BOOK_VARIATIONS[BOOK_VARIATION_COUNT];

#pragma region USING_STATEMENTS

	using ItemDefinitionDict = std::map<std::string, ItemDefinition>;

	using ItemStackList = std::vector<ItemStackRef>;
	using EmptyStackRsrv = std::queue<ItemStackList::iterator>;

	using ItemVariationPair = std::pair<const std::string*, int>;
	using ItemVariationDict = std::map<ItemType, ItemVariationPair>;
	using MappedVariationDict =
	  std::map<ItemType, std::map<std::string, int>>;

#pragma endregion USING_STATEMENTS

	class ItemHandler {
	private:
		static ItemDefinitionDict itemDefintions;
		static ItemStackList itemStacks;
		static EmptyStackRsrv emptyStacks;
		static ItemVariationDict itemVariations;
		static MappedVariationDict mappedVariations;

		static void registerItem(std::string name, Item* item);
		static void registerVariations(ItemType type,
					       const std::string* variationList,
					       int count);
		static void mapVariations();

	public:
		static void init();
		static ItemStackRef createItemStack(std::string name,
						    unsigned short qty = 1,
						    unsigned long flags = 0L);
		static void removeStack(ItemStackRef stack);
		static void draw();
		static void update();
	};
}
#endif