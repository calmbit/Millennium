/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_ITEMSTACK_HPP
#define MLNM_ITEMSTACK_HPP

#include "entity.hpp"
#include "item.hpp"

namespace mlnm {

	const int STACK_IN_INVENTORY = -1;

	// clang-format off
	enum class ItemStackFlag : unsigned long {
		ROTTEN		= 0x0000000000000001L, // item has rotted
		BUC_KNOWN 	= 0x0000000000000002L, // blessed/uncursed/cursed status known
		LOCKED		= 0x0000000000000004L, // item has been locked
		LOCK_BROKEN = 0x0000000000000008L, // lock was broken
		LOCK_KNOWN  = 0x0000000000000010L, // lock status known
		TYPE_KNOWN  = 0x0000000000000020L  // type known (spellbook, wand, ring, etc.)
	};
	// clang-format on

	enum class ItemStackDisposition { CURSED, UNCURSED, BLESSED };

	class ItemStack : public std::enable_shared_from_this<ItemStack> {
	private:
		unsigned char tile;
		IndexedColor color;
		unsigned short qty;
		unsigned long flags;
		ItemDefinition item;
		ItemStackDisposition disposition;
		std::string stackString;
		int x, y;

	public:
		ItemStack(ItemDefinition item,
			  unsigned short qty,
			  unsigned long flags);
		std::shared_ptr<ItemStack> getSharedRef();
		void setDisposition(ItemStackDisposition disposition);
		bool isBlessed();
		bool isCursed();
		bool isUncursed();
		bool onUse(EntityInstanceRef ent);
		void set(ItemStackFlag flag);
		bool is(ItemStackFlag flag);
		ItemDefinition getItem();
		void updateStack();
		std::string getName();
		std::string getStackString();
		void consumeOne();
		void empty();
		bool isEmpty();
		void overwite(ItemDefinition item,
			      unsigned short qty = 1,
			      unsigned long flags = 0L);
		void setPosition(int x, int y);
		void draw();
	};
}

#endif