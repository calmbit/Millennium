/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_LOCALIZATION_HPP
#define MLNM_LOCALIZATION_HPP

#include <map>
#include <string>

#include "json.hpp"

using nlohmann::json;

namespace mlnm {
	const std::string UNDEFINED_TOKEN = "undefined";

	struct LocaleFile {
		std::string locale;
		std::map<std::string, std::string> tokens;
		LocaleFile();
		~LocaleFile();
		std::string getToken(std::string tokenName);
	};

	void to_json(json& j, const LocaleFile& file);
	void from_json(const json& j, LocaleFile& file);

	extern LocaleFile globalLocale;

	std::string getLocaleToken(std::string tokenName);
} // namespace mlnm

#endif