/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_LOGGER_HPP
#define MLNM_LOGGER_HPP

#include <sstream>

namespace mlnm {
	enum class LoggerLevel {
		TRACE,
		DEBUG,
		INFO,
		WARN,
		ERROR,
		FATAL,
		SILENT
	};

	extern const char*
	  LOGGER_LEVEL_STRINGS[static_cast<int>(LoggerLevel::SILENT)];

	class Logger {
	private:
		static LoggerLevel filterLevel;

	public:
		static void setFilterLevel(LoggerLevel newFilter);
		static void log(LoggerLevel logLevel,
				const char* location,
				const char* msg);
		static void trace(const char* location, const char* msg);
		static void debug(const char* location, const char* msg);
		static void info(const char* location, const char* msg);
		static void warn(const char* location, const char* msg);
		static void error(const char* location, const char* msg);
		static void fatal(const char* location, const char* msg);
		static void loggerTest();
	};

	class LogEntryBuilder {
	private:
		LoggerLevel level;
		std::stringstream buffer;
		std::string location;

	public:
		LogEntryBuilder(LoggerLevel level, const std::string& location);
		~LogEntryBuilder();
		LogEntryBuilder& addString(const std::string& str);
		LogEntryBuilder& addString(const char* str);
		LogEntryBuilder& addInt(int val);
		LogEntryBuilder& addFloat(float val);
		void log();
	};
} // namespace mlnm

#endif
