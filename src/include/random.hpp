/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_RANDOM_HPP
#define MLNM_RANDOM_HPP

#include <random>

namespace mlnm {
	class Random {
	private:
		static std::random_device rd;
		static std::mt19937 mte;
		static std::uniform_int_distribution<> coinFlip, d3, d4, d6,
		  d10, d12, d20, d100;
		static bool initialized;
		static int rollDie(int rolls,
				   std::uniform_int_distribution<> distr);

	public:
		static int rolld4(int rolls);
		static int rolld6(int rolls);
		static int rolld10(int rolls);
		static int rolld12(int rolls);
		static int rolld20(int rolls);
		static int rolld100(int rolls);
		static int rolld3(int rolls);
		static int flipCoin(int flips);
		static int rollCustomDie(int rolls, int pips);
	};
} // namespace mlnm

#endif