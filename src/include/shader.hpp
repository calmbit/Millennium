/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_SHADER_HPP
#define MLNM_SHADER_HPP

#include <string>

#define GLEW_STATIC
#include <GL/glew.h>

namespace mlnm {
	const int SHADER_ERROR_LOG_SIZE = 512;
	extern const char* FRAGMENT_OUT_COLOR_NAME;
	extern const char* VERTEX_POSITION_NAME;
	extern const char* SHADER_ROOT;
	extern const char* FRAGMENT_FILE;
	extern const char* VERTEX_FILE;

	class ShaderBase {
	private:
		bool failbit = false;
		GLuint id;
		GLuint shaderType;

	public:
		ShaderBase();
		ShaderBase(const std::string& path, GLuint shaderType);
		~ShaderBase();
		int getId() { return this->id; }
		bool good() { return !this->failbit; }
	};

	class VertexShader : public ShaderBase {
	public:
		VertexShader();
		explicit VertexShader(const std::string& path);
		~VertexShader();
	};

	class FragmentShader : public ShaderBase {
	public:
		FragmentShader();
		explicit FragmentShader(const std::string& path);
		~FragmentShader();
	};

	// TODO: Convert to class with getter for ID?
	struct ShaderProgram {
		GLuint id;
		ShaderProgram();
		ShaderProgram(VertexShader& vs, FragmentShader& fs);
		~ShaderProgram();
		void use();
		void newAttrib(const char* name, int size, GLuint type);
	};
} // namespace mlnm

#endif
