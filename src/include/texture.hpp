/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_TEXTURE_HPP
#define MLNM_TEXTURE_HPP

#include <string>

#include <SOIL.h>

#include "shader.hpp"

namespace mlnm {
	struct Texture {
		int width, height;
		GLuint id;
		Texture();
		explicit Texture(std::string path);
		~Texture();
	};
} // namespace mlnm

#endif
