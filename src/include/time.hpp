/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_TIME_HPP
#define MLNM_TIME_HPP

namespace mlnm {

	enum WeekDay {
		SUNDAY,
		MONDAY,
		TUESDAY,
		WEDNESDAY,
		THURSDAY,
		FRIDAY,
		SATURDAY
	};

	enum PlanetPhase {
		NEW,
		WAX_CRES,
		FIR_QUAR,
		WAX_GIBB,
		FULL,
		WAN_GIBB,
		LAS_QUAR,
		WAN_CRES
	};

	class DateTime {
	private:
		unsigned int second, minute, hour, day, month, year, dayOfYear;
		WeekDay dayOfWeek;

	public:
		DateTime();

		static PlanetPhase getAlagaPhase();
		static PlanetPhase getMalensPhase();
		static PlanetPhase getTenserPhase();
		static PlanetPhase getBentkaPhase();
	};
}

#endif