/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MLNM_WINDOW_HPP
#define MLNM_WINDOW_HPP

#include <SDL.h>

#include <functional>
#include <map>
#include <vector>

#include "font.hpp"
#include "texture.hpp"

namespace mlnm {
	const int MLNM_GL_MAJOR_VERSION = 3;
	const int MLNM_GL_MINOR_VERSION = 2;
	const int MLNM_GL_STENCIL_SIZE = 8;

	const int MULTI_X = COORDS_PER_TILE;
	const int MULTI_Y = ROWS_X * COORDS_PER_TILE;

	extern std::string fontName;

	class Window {
	private:
		static SDL_Window* window;
		static SDL_GLContext context;
		static ShaderProgram program;
		static bool destroyed;
		static int lastFps;
		static int avgFps;
		static int fpsAggregate;
		static int fpsCounter;
		static int fpsTimer;
		static std::map<int,
				std::vector<std::function<void(SDL_Event*)>>>
		  callbacks;

	public:
		static void setup();
		static void bindCallback(int type,
					 std::function<void(SDL_Event*)> func);
		static void loop();
		static void draw();
		static void destroy();
	};
} // namespace mlnm

#endif