/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "item.hpp"

#include <functional>
#include <memory>

#include "logger.hpp"

namespace mlnm {

	const std::string STANDARD_PLURAL = "{SP}";
	const std::string ALREADY_PLURAL = "{AP}";

	Item::Item() {
		this->name = "undefined";
		this->plural = "undefined";
		this->type = ItemType::UNKNOWN;
		this->tile = '\000';
		this->color = IndexedColor{};
		this->useDelegate = [](EntityInstanceRef, ItemStackRef) {
			return false;
		};
		this->element = AlignmentElement::NEUTRUM;
	}

	Item::Item(std::string name,
		   std::string plural,
		   ItemType type,
		   unsigned char tile,
		   IndexedColor color,
		   ItemUseDelegate useDelegate,
		   AlignmentElement element) {
		this->name = name;
		this->plural =
		  (plural == STANDARD_PLURAL ? this->name + "s" : plural);
		this->type = type;
		this->tile = tile;
		this->color = color;
		this->useDelegate = useDelegate;
		this->element = element;
	}

	Item::~Item() {}

	std::string Item::getName() { return this->name; }

	std::string Item::getPlural() { return this->plural; }

	unsigned char Item::getTile() { return this->tile; }

	IndexedColor& Item::getColor() { return this->color; }

	bool Item::onUse(EntityInstanceRef ent, ItemStackRef stack) {
		return this->useDelegate(ent, stack);
	}
}