/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "itemfood.hpp"

#include "entityinstance.hpp"
#include "itemstack.hpp"

#include "logger.hpp"

namespace mlnm {
	ItemFood::ItemFood(std::string name,
			   std::string plural,
			   IndexedColor color,
			   int foodValue,
			   ItemFood::SubClass subClass)
	  : Item(name,
		 plural,
		 ItemType::FOOD,
		 '%',
		 color,
		 std::bind(&ItemFood::eat,
			   this,
			   std::placeholders::_1,
			   std::placeholders::_2)) {
		this->foodValue = foodValue;
		this->subClass = subClass;
	}

	ItemFood::~ItemFood() {}

	bool ItemFood::eat(EntityInstanceRef, ItemStackRef stack) {

		ItemFoodDefinition food =
		  std::dynamic_pointer_cast<ItemFood>(stack->getItem());
		if (stack->is(ItemStackFlag::ROTTEN)) {
			Logger::info("item_food", "Ugh, this is rotten!");
			// TODO: Rot logic.
		} else {
			switch (food->subClass) {
				case ItemFood::SubClass::APPLE: {
					Logger::info("item_food:apple",
						     "Core dumped.");
					break;
				}
				case ItemFood::SubClass::RATION: {
					Logger::info("item_food:ration",
						     "Something something you "
						     "ate a ration.");
					break;
				}
			}
		}

		stack->consumeOne();
		return true;
	}
}