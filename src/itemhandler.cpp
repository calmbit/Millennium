/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "itemhandler.hpp"

#include "itemfood.hpp"
#include "logger.hpp"

namespace mlnm {

	const std::string RING_VARIATIONS[RING_VARIATION_COUNT] = {
		"garnet",   "amethyst",  "aquamarine", "diamond",
		"emerald",  "moonstone", "ruby",       "peridot",
		"sapphire", "opal",      "topaz",      "tanzanite"
	};

	const std::string BOOK_VARIATIONS[BOOK_VARIATION_COUNT] = {
		"blue",  "green", " yellow", "orange", "puple",    "magenta",
		"brown", "red",   "white",   "black",  "tattered", "pale"
	};

	ItemDefinitionDict ItemHandler::itemDefintions;
	ItemVariationDict ItemHandler::itemVariations;
	MappedVariationDict ItemHandler::mappedVariations;
	ItemStackList ItemHandler::itemStacks;
	EmptyStackRsrv ItemHandler::emptyStacks;

	void ItemHandler::registerItem(std::string name, Item* item) {
		itemDefintions.insert(
		  std::make_pair(name, ItemDefinition{ std::move(item) }));
	}

	void ItemHandler::registerVariations(ItemType type,
					     const std::string* variation,
					     int count) {
		if (itemVariations.find(type) == itemVariations.end()) {
			itemVariations.insert(std::make_pair(
			  type, ItemVariationPair{ variation, count }));
		}
	}

	void ItemHandler::init() {
		itemDefintions = ItemDefinitionDict{};
		itemVariations = ItemVariationDict{};
		mappedVariations = MappedVariationDict{};
		itemStacks = ItemStackList{};
		emptyStacks = EmptyStackRsrv{};

		registerVariations(
		  ItemType::RING, RING_VARIATIONS, RING_VARIATION_COUNT);
		registerVariations(
		  ItemType::SPELLBOOK, BOOK_VARIATIONS, BOOK_VARIATION_COUNT);

		registerItem("apple",
			     new ItemFood{ "apple",
					   STANDARD_PLURAL,
					   IndexedColor{ 4, 0, 1 },
					   20,
					   ItemFood::SubClass::APPLE });
		registerItem("pear",
			     new ItemFood{ "pear",
					   STANDARD_PLURAL,
					   IndexedColor{ 6, 0, 1 },
					   20,
					   ItemFood::SubClass::APPLE });
		registerItem("soldier's ration",
			     new ItemFood{ "soldier's ration",
					   STANDARD_PLURAL,
					   IndexedColor{ 6, 0, 0 },
					   60,
					   ItemFood::SubClass::RATION });
	}

	ItemStackRef ItemHandler::createItemStack(std::string name,
						  unsigned short qty,
						  unsigned long flags) {
		// Make sure our item exists in the definitions map.
		if (itemDefintions.find(name) != itemDefintions.end()) {

			// We've got a valid item reference.
			auto item = itemDefintions.at(name);

			// Optimization - if we've got some empty stacks, then
			// let's use them to our advantage.
			if (!emptyStacks.empty()) {
				auto sid = emptyStacks.front();
				emptyStacks.pop();
				(*sid)->overwite(item, qty, flags);
				return (*sid)->getSharedRef();
			}
			// Otherwise, we'll be forced to create new data.
			else {
				itemStacks.push_back(
				  std::make_shared<ItemStack>(
				    item, qty, flags));
				return itemStacks.back()->getSharedRef();
			}
		}
		// Hmm. Looks like something went wrong.
		else {
			LogEntryBuilder{ LoggerLevel::FATAL, "entity_handler" }
			  .addString("Tried to create item stack for "
				     "non-existant item '")
			  .addString(name)
			  .addString("' - aborting")
			  .log();
			return nullptr;
		}
	}

	void ItemHandler::mapVariations() {}

	void ItemHandler::removeStack(ItemStackRef stack) {
		auto i = std::find(itemStacks.begin(), itemStacks.end(), stack);
		if (i != itemStacks.end()) {
			(*i)->empty();
			emptyStacks.push(i);
		} else {
			Logger::fatal("item_handler",
				      "Tried to remove an invalid item stack - "
				      "mishandled pointer, somewhere");
		}
	}

	void ItemHandler::draw() {
		for (auto f : itemStacks) {
			f->draw();
		}
	}

	void ItemHandler::update() {}
}