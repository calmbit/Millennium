/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "itemstack.hpp"

#include "font.hpp"

namespace mlnm {

	ItemStack::ItemStack(ItemDefinition item,
			     unsigned short qty,
			     unsigned long flags) {
		this->item = item;
		this->qty = qty;
		this->flags = flags;
		this->disposition = ItemStackDisposition::UNCURSED;
		this->x = STACK_IN_INVENTORY;
		this->y = STACK_IN_INVENTORY;
		this->updateStack();
	}

	std::shared_ptr<ItemStack> ItemStack::getSharedRef() {
		return shared_from_this();
	}

	bool ItemStack::onUse(EntityInstanceRef ent) {
		return item->onUse(ent, getSharedRef());
	}

	ItemDefinition ItemStack::getItem() { return this->item; }

	void ItemStack::setDisposition(ItemStackDisposition disposition) {
		this->disposition = disposition;
	}

	void ItemStack::updateStack() {
		std::stringstream buff{};
		buff << this->qty << " ";
		if (this->is(ItemStackFlag::BUC_KNOWN)) {
			buff << (this->isBlessed()
				   ? "blessed "
				   : (this->isUncursed() ? "uncursed "
							 : "cursed "));
		}
		buff << (this->qty == 1 ? this->item->getName()
					: this->item->getPlural());

		this->stackString = buff.str();

		this->tile = item->getTile();
		this->color = item->getColor();
	}

	bool ItemStack::isBlessed() {
		return this->disposition == ItemStackDisposition::BLESSED;
	}
	bool ItemStack::isCursed() {
		return this->disposition == ItemStackDisposition::CURSED;
	}
	bool ItemStack::isUncursed() {
		return this->disposition == ItemStackDisposition::UNCURSED;
	}

	std::string ItemStack::getName() { return this->item->getName(); }

	std::string ItemStack::getStackString() {
		return this->isEmpty() ? "nothing" : this->stackString;
	}

	void ItemStack::set(ItemStackFlag flag) {
		this->flags = this->flags | static_cast<unsigned long>(flag);
		this->updateStack();
	}

	bool ItemStack::is(ItemStackFlag flag) {
		return (this->flags & static_cast<unsigned long>(flag)) != 0;
	}

	void ItemStack::consumeOne() {
		this->qty--;
		if (this->qty <= 0) {
			this->empty();
		} else {
			this->updateStack();
		}
	}
	void ItemStack::empty() {
		this->qty = 0;
		this->item = nullptr;
		this->tile = '\0';
		this->color = IndexedColor{};
		this->flags = 0L;
		this->disposition = ItemStackDisposition::UNCURSED;
		this->stackString = "";
	}

	bool ItemStack::isEmpty() { return this->item == nullptr; }

	void ItemStack::setPosition(int x, int y) {
		this->x = x;
		this->y = y;
	}

	void ItemStack::overwite(ItemDefinition item,
				 unsigned short qty,
				 unsigned long flags) {
		this->item = item;
		this->qty = qty;
		this->flags = flags;
		this->disposition = ItemStackDisposition::UNCURSED;
		this->stackString = "";
		this->updateStack();
	}

	void ItemStack::draw() {
		if (this->x == STACK_IN_INVENTORY ||
		    this->y == STACK_IN_INVENTORY)
			return;
		FontManager::addTileToRender(this->tile,
					     this->x,
					     this->y,
					     this->color.fg,
					     this->color.bg,
					     this->color.bd);
	}
}