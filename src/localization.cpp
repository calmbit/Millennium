/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "localization.hpp"

#include <stdexcept>

namespace mlnm {
	LocaleFile globalLocale;

	LocaleFile::LocaleFile() {}

	LocaleFile::~LocaleFile() {}

	void to_json(json& j, const LocaleFile& file) {
		j =
		  json{ { "locale", file.locale }, { "tokens", file.tokens } };
	}

	void from_json(const json& j, LocaleFile& file) {
		file.locale = j.at("locale").get<std::string>();
		file.tokens =
		  j.at("tokens").get<std::map<std::string, std::string>>();
	}

	std::string LocaleFile::getToken(std::string tokenName) {
		try {
			return this->tokens.at(tokenName);
		} catch (std::out_of_range& e) {
			return UNDEFINED_TOKEN;
		}
	}

	std::string getLocaleToken(std::string tokenName) {
		return mlnm::globalLocale.getToken(tokenName);
	}
} // namespace mlnm
