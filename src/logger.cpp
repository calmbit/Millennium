/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "logger.hpp"

#include <iostream>

namespace mlnm {
	LoggerLevel Logger::filterLevel;

	const char*
	  LOGGER_LEVEL_STRINGS[static_cast<int>(LoggerLevel::SILENT)] = {
		  "\033[37;1mTRACE\033[0m", "\033[35;1mDEBUG\033[0m",
		  "\033[34;1mINFO\033[0m",  "\033[33;1mWARN\033[0m",
		  "\033[31;1mERROR\033[0m", "\033[31;7mFATAL\033[0m"
	  };

	void Logger::setFilterLevel(LoggerLevel newFilter) {
		filterLevel = newFilter;
	}

	void Logger::log(LoggerLevel logLevel,
			 const char* location,
			 const char* msg) {
		if (static_cast<int>(logLevel) < static_cast<int>(filterLevel))
			return;

		(logLevel >= LoggerLevel::WARN ? std::cerr : std::cout)
		  << "[" << LOGGER_LEVEL_STRINGS[static_cast<int>(logLevel)]
		  << "] "
		  << "(" << location << ") " << msg << std::endl;
	}

	void Logger::trace(const char* location, const char* msg) {
		log(LoggerLevel::TRACE, location, msg);
	}

	void Logger::debug(const char* location, const char* msg) {
		log(LoggerLevel::DEBUG, location, msg);
	}

	void Logger::info(const char* location, const char* msg) {
		log(LoggerLevel::INFO, location, msg);
	}

	void Logger::warn(const char* location, const char* msg) {
		log(LoggerLevel::WARN, location, msg);
	}

	void Logger::error(const char* location, const char* msg) {
		log(LoggerLevel::ERROR, location, msg);
	}

	void Logger::fatal(const char* location, const char* msg) {
		log(LoggerLevel::FATAL, location, msg);
	}

	void Logger::loggerTest() {
		trace("test", "This is a trace!");
		debug("test", "This is a debug statement!");
		info("test", "This is some info!");
		warn("test", "This is a warning!");
		error("test", "This is an error!!");
		fatal("test", "This is a FATAL error!");
	}

	LogEntryBuilder::LogEntryBuilder(LoggerLevel level,
					 const std::string& location) {
		this->level = level;
		this->buffer = std::stringstream{};
		this->location = location;
	}
	LogEntryBuilder::~LogEntryBuilder() {}
	LogEntryBuilder& LogEntryBuilder::addString(const std::string& str) {
		this->buffer << str;
		return *this;
	}
	LogEntryBuilder& LogEntryBuilder::addString(const char* str) {
		this->buffer << str;
		return *this;
	}
	LogEntryBuilder& LogEntryBuilder::addInt(int val) {
		this->buffer << val;
		return *this;
	}
	LogEntryBuilder& LogEntryBuilder::addFloat(float val) {
		this->buffer << val;
		return *this;
	}
	void LogEntryBuilder::log() {
		Logger::log(this->level,
			    this->location.c_str(),
			    this->buffer.str().c_str());
	}
} // namespace mlnm
