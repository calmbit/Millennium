/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#define MLNM_DEBUG

#include <chrono>
#include <fstream>
#include <iostream>

#include "color.hpp"
#include "localization.hpp"
#include "logger.hpp"
#include "random.hpp"
#include "window.hpp"

using nlohmann::json;

int
main() {

#ifdef MLNM_DEBUG
	mlnm::Logger::setFilterLevel(mlnm::LoggerLevel::DEBUG);
#else
	mlnm::Logger::setFilterLevel(mlnm::LoggerLevel::INFO);
#endif

	mlnm::Logger::info("main", "Starting up...");

	mlnm::Logger::debug("main", "Loading configuration...");
	std::ifstream confFile{ "def/config/conf.json" };

	json confJson;
	confFile >> confJson;

	std::string localeFile =
	  "def/locale/" + confJson.at("locale").get<std::string>() + ".json";
	std::string colorFile =
	  "def/colors/" + confJson.at("colors").get<std::string>() + ".json";

	mlnm::fontName = "def/fonts/" + confJson.at("font").get<std::string>();

	mlnm::Logger::debug("main", "Loading locale file...");

	std::ifstream localeFileStream{ localeFile };

	if (!localeFileStream.good()) {
		mlnm::Logger::fatal("main", "Unable to load locale file.");
		return -1;
	}

	json j;
	localeFileStream >> j;

	localeFileStream.close();

	mlnm::globalLocale = j;

	mlnm::Logger::debug("main", "Loading colors file...");

	std::ifstream colorFileStream{ colorFile };

	if (!colorFileStream.good()) {
		mlnm::Logger::fatal("main", "Unable to load colors file.");
		return -1;
	}

	json colorJson;
	colorFileStream >> colorJson;

	colorFileStream.close();

	mlnm::globalColors = colorJson;

	mlnm::Window::setup();
	mlnm::Window::loop();
	mlnm::Window::destroy();
}
