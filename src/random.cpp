/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "random.hpp"

namespace mlnm {
	std::random_device Random::rd;
	std::mt19937 Random::mte{ rd() };
	std::uniform_int_distribution<> Random::coinFlip{ 0, 1 };
	std::uniform_int_distribution<> Random::d3{ 1, 3 };
	std::uniform_int_distribution<> Random::d4{ 1, 4 };
	std::uniform_int_distribution<> Random::d6{ 1, 6 };
	std::uniform_int_distribution<> Random::d10{ 1, 10 };
	std::uniform_int_distribution<> Random::d12{ 1, 12 };
	std::uniform_int_distribution<> Random::d20{ 1, 20 };
	std::uniform_int_distribution<> Random::d100{ 1, 100 };

	int Random::rollDie(int rolls, std::uniform_int_distribution<> distr) {
		int f = 0;

		for (auto i = 0; i < rolls; i++) {
			f += distr(Random::mte);
		}

		return f;
	}

	int Random::rolld4(int rolls) { return rollDie(rolls, Random::d4); }

	int Random::rolld6(int rolls) { return rollDie(rolls, Random::d6); }

	int Random::rolld10(int rolls) { return rollDie(rolls, Random::d10); }

	int Random::rolld12(int rolls) { return rollDie(rolls, Random::d12); }

	int Random::rolld20(int rolls) { return rollDie(rolls, Random::d20); }

	int Random::rolld100(int rolls) { return rollDie(rolls, Random::d100); }

	int Random::rollCustomDie(int rolls, int pips) {
		return rollDie(rolls,
			       std::uniform_int_distribution<>{ 1, pips });
	}

	int Random::rolld3(int rolls) { return rollDie(rolls, Random::d3); }
	int Random::flipCoin(int flips) {
		return rollDie(flips, Random::coinFlip);
	}
} // namespace mlnm