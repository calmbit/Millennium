/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shader.hpp"

#include <fstream>
#include <iostream>

#include "logger.hpp"

namespace mlnm {
	const char* FRAGMENT_OUT_COLOR_NAME = "outColor";
	const char* VERTEX_POSITION_NAME = "position";
	const char* SHADER_ROOT = "def/shader/";
	const char* FRAGMENT_FILE = "def/shader/fragment.hlsl";
	const char* VERTEX_FILE = "def/shader/vertex.hlsl";

	ShaderBase::ShaderBase() {
		this->shaderType = 0;
		this->failbit = true;
		this->shaderType = 0;
	}

	ShaderBase::ShaderBase(const std::string& path, GLuint shaderType) {
		std::ifstream inFile{ path };
		char* buffer;

		this->shaderType = shaderType;

		if (!inFile.good()) {
			LogEntryBuilder{ LoggerLevel::FATAL, "shader" }
			  .addString("Unable to load shader at ")
			  .addString(path)
			  .addString(" - aborting")
			  .log();
			this->failbit = true;
			return;
		}

		inFile.seekg(0, std::ifstream::end);
		int fileSize = inFile.tellg();
		buffer = new char[fileSize + 1];
		inFile.seekg(0, std::ifstream::beg);
		inFile.read(buffer, fileSize);
		inFile.close();

		buffer[fileSize] = '\0';

		this->id = glCreateShader(shaderType);
		glShaderSource(this->id, 1, &buffer, NULL);
		glCompileShader(this->id);
		delete[] buffer;
		buffer = nullptr;

		GLint status;
		glGetShaderiv(this->id, GL_COMPILE_STATUS, &status);

		if (status != GL_TRUE) {
			char errBuffer[SHADER_ERROR_LOG_SIZE];
			glGetShaderInfoLog(
			  this->id, SHADER_ERROR_LOG_SIZE, NULL, errBuffer);

			LogEntryBuilder{ LoggerLevel::FATAL, "shader" }
			  .addString((shaderType == GL_VERTEX_SHADER
					? "Vertex"
					: "Fragment"))
			  .addString(" shader ran into compilation issues")
			  .log();

			Logger::fatal("shader", errBuffer);

			this->failbit = true;
			return;
		}

		this->failbit = false;

		LogEntryBuilder{ LoggerLevel::INFO, "shader" }
		  .addString(
		    (shaderType == GL_VERTEX_SHADER ? "Vertex" : "Fragment"))
		  .addString(" shader compiled successfully")
		  .log();
	}

	ShaderBase::~ShaderBase() {}

	VertexShader::VertexShader()
	  : ShaderBase() {}

	VertexShader::VertexShader(const std::string& path)
	  : ShaderBase(path, GL_VERTEX_SHADER) {}

	VertexShader::~VertexShader() {}

	FragmentShader::FragmentShader()
	  : ShaderBase() {}

	FragmentShader::FragmentShader(const std::string& path)
	  : ShaderBase(path, GL_FRAGMENT_SHADER) {}

	FragmentShader::~FragmentShader() {}

	ShaderProgram::ShaderProgram() {}

	ShaderProgram::ShaderProgram(VertexShader& vs, FragmentShader& fs) {
		if (!vs.good() || !fs.good()) {
			Logger::fatal(
			  "shader",
			  "Sanity check failed - shaders previously failed "
			  "to compile, cannot compile shader program");
			return;
		}

		this->id = glCreateProgram();
		glAttachShader(this->id, vs.getId());
		glAttachShader(this->id, fs.getId());
		glBindFragDataLocation(this->id, 0, FRAGMENT_OUT_COLOR_NAME);
		glLinkProgram(this->id);
		this->use();

		Logger::info(
		  "shader",
		  "Shader program appears to have been linked correctly");
	}

	ShaderProgram::~ShaderProgram() {}

	void ShaderProgram::use() { glUseProgram(this->id); }

	void ShaderProgram::newAttrib(const char* name, int size, GLuint type) {
		GLint attrib = glGetAttribLocation(this->id, name);
		if (type == GL_INT) {
			glVertexAttribIPointer(attrib, size, type, 0, 0);
		} else {
			glVertexAttribPointer(
			  attrib, size, type, GL_FALSE, 0, 0);
		}
		glEnableVertexAttribArray(attrib);
	}
} // namespace mlnm
