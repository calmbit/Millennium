/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "texture.hpp"

#include <iostream>

namespace mlnm {
	Texture::Texture() {
		this->id = 0;
		this->height = -1;
		this->width = -1;
	}

	Texture::Texture(std::string path) {
		glGenTextures(1, &this->id);
		glBindTexture(GL_TEXTURE_2D, this->id);
		unsigned char* data = SOIL_load_image(
		  path.c_str(), &this->width, &this->height, 0, SOIL_LOAD_RGBA);
		glTexImage2D(GL_TEXTURE_2D,
			     0,
			     GL_RGBA,
			     width,
			     height,
			     0,
			     GL_RGBA,
			     GL_UNSIGNED_BYTE,
			     data);
		SOIL_free_image_data(data);

		glTexParameteri(
		  GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(
		  GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(
		  GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(
		  GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	Texture::~Texture() {}
} // namespace mlnm