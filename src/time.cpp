/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "time.hpp"

#include <ctime>

namespace mlnm {
	DateTime::DateTime() {
		auto t = std::time(nullptr);
		auto tm = std::gmtime(&t);

		this->second = tm->tm_sec;
		this->minute = tm->tm_min;
		this->hour = tm->tm_hour;
		this->day = tm->tm_mday;
		this->month = tm->tm_mon;
		this->year = tm->tm_year;
		this->dayOfWeek = static_cast<WeekDay>(tm->tm_wday);
		this->dayOfYear = tm->tm_yday;
	}

	PlanetPhase DateTime::getAlagaPhase() {
		return NEW; // unimplemented
	}

	PlanetPhase DateTime::getMalensPhase() {
		return NEW; // unimplemented
	}

	PlanetPhase DateTime::getTenserPhase() {
		return NEW; // unimplemented
	}

	PlanetPhase DateTime::getBentkaPhase() {
		return NEW; // unimplemented
	}
}