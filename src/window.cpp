/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "window.hpp"

#include <SDL_video.h>
#include <fstream>
#include <iostream>
#include <random>

#include "color.hpp"
#include "entityhandler.hpp"
#include "entityinstance.hpp"
#include "entityplayerinstance.hpp"
#include "event.hpp"
#include "item.hpp"
#include "itemfood.hpp"
#include "itemhandler.hpp"
#include "localization.hpp"
#include "logger.hpp"

namespace mlnm {
	// TODO: Clean this up, stop polluting the namespace

	// For Verticies

	int vertColours[ROWS_X * ROWS_Y * VERTS_PER_TILE * COLOR_COMPONENTS];
	float texCoords[ROWS_X * ROWS_Y * COORDS_PER_TILE];
	float verticies[ROWS_X * ROWS_Y * COORDS_PER_TILE];

	IndexedColor playerColor{ 3, 0, 1 };

	SDL_Window* Window::window;
	SDL_GLContext Window::context;
	ShaderProgram Window::program;
	bool Window::destroyed = false;
	int Window::lastFps = 0;
	int Window::avgFps = 0;
	int Window::fpsAggregate = 0;
	int Window::fpsCounter = 0;
	int Window::fpsTimer = 0;

	std::string fontName;

	std::map<int, std::vector<std::function<void(SDL_Event*)>>>
	  Window::callbacks{};

	void Window::setup() {
		Logger::debug("window", "Initializing SDL...");
		SDL_Init(SDL_INIT_EVERYTHING);

		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
				    SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,
				    MLNM_GL_MAJOR_VERSION);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,
				    MLNM_GL_MINOR_VERSION);
		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, MLNM_GL_STENCIL_SIZE);

		Logger::debug("window", "Initializing window...");
		window = SDL_CreateWindow(getLocaleToken("mlnm_name").c_str(),
					  100,
					  100,
					  0,
					  0,
					  SDL_WINDOW_OPENGL);
		Logger::debug("window", "Initializing OpenGL context");
		context = SDL_GL_CreateContext(window);

		Logger::debug("window", "Initializing GLEW...");
		glewExperimental = GL_TRUE;
		glewInit();

		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_SRC_ALPHA);

		Logger::debug("window", "Setting up vertex definitions...");
		for (auto y = 0; y < ROWS_Y; y++) {
			for (auto x = 0; x < ROWS_X; x++) {

				// Verts

				verticies[(y * MULTI_Y) + (x * MULTI_X)] =
				  (x * VERT_INCREMENT_X) - 1.0f;
				verticies[(y * MULTI_Y) + (x * MULTI_X) + 1] =
				  (y * VERT_INCREMENT_Y) + 1.0f;

				verticies[(y * MULTI_Y) + (x * MULTI_X) + 2] =
				  ((x + 1) * VERT_INCREMENT_X) - 1.0f;
				verticies[(y * MULTI_Y) + (x * MULTI_X) + 3] =
				  (y * VERT_INCREMENT_Y) + 1.0f;

				verticies[(y * MULTI_Y) + (x * MULTI_X) + 4] =
				  ((x + 1) * VERT_INCREMENT_X) - 1.0f;
				verticies[(y * MULTI_Y) + (x * MULTI_X) + 5] =
				  ((y + 1) * VERT_INCREMENT_Y) + 1.0f;

				verticies[(y * MULTI_Y) + (x * MULTI_X) + 6] =
				  ((x + 1) * VERT_INCREMENT_X) - 1.0f;
				verticies[(y * MULTI_Y) + (x * MULTI_X) + 7] =
				  ((y + 1) * VERT_INCREMENT_Y) + 1.0f;

				verticies[(y * MULTI_Y) + (x * MULTI_X) + 8] =
				  ((x)*VERT_INCREMENT_X) - 1.0f;
				verticies[(y * MULTI_Y) + (x * MULTI_X) + 9] =
				  ((y + 1) * VERT_INCREMENT_Y) + 1.0f;

				verticies[(y * MULTI_Y) + (x * MULTI_X) + 10] =
				  (x * VERT_INCREMENT_X) - 1.0f;
				verticies[(y * MULTI_Y) + (x * MULTI_X) + 11] =
				  (y * VERT_INCREMENT_Y) + 1.0f;
			}
		}

		Logger::debug("window", "Generating and binding VAO...");
		GLuint vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		Logger::debug("window",
			      "Generating and binding vertex buffer...");
		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER,
			     sizeof(verticies),
			     verticies,
			     GL_STATIC_DRAW);

		Logger::debug("window",
			      "Generating and binding color buffer...");
		GLuint cbo;
		glGenBuffers(1, &cbo);
		glBindBuffer(GL_ARRAY_BUFFER, cbo);
		glBufferData(GL_ARRAY_BUFFER,
			     sizeof(vertColours),
			     vertColours,
			     GL_STREAM_DRAW);

		Logger::debug("window",
			      "Generating and binding texcoord buffer...");
		GLuint tbo;
		glGenBuffers(1, &tbo);
		glBindBuffer(GL_ARRAY_BUFFER, tbo);
		glBufferData(GL_ARRAY_BUFFER,
			     sizeof(texCoords),
			     texCoords,
			     GL_STATIC_DRAW);

		Logger::debug("window", "Loading vertex shader...");
		VertexShader vertexShader{ VERTEX_FILE };
		Logger::debug("window", "Loading fragment shader...");
		FragmentShader fragmentShader{ FRAGMENT_FILE };

		if (!vertexShader.good() || !fragmentShader.good()) {
			destroy();
			return;
		}

		Logger::debug("window",
			      "Creating and compiling shader program...");
		program = ShaderProgram{ vertexShader, fragmentShader };

		Logger::debug("window", "Setting up vertex attributes...");
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		program.newAttrib(VERTEX_POSITION_NAME, 2, GL_FLOAT);
		glBindBuffer(GL_ARRAY_BUFFER, cbo);
		program.newAttrib("color", 3, GL_INT);
		glBindBuffer(GL_ARRAY_BUFFER, tbo);
		program.newAttrib("texcoord", 2, GL_FLOAT);

		Logger::debug("window", "Generating color definitions...");
		float* color_defs = mlnm::globalColors.getColors();

		Logger::debug("window", "Filling color definition uniform...");
		GLint colors = glGetUniformLocation(program.id, "colors");
		glUniform3fv(colors, COLOR_COUNT, color_defs);

		delete[](color_defs);

		Logger::debug("window", "Loading font texture...");
		Texture font{ fontName };

		SDL_SetWindowSize(window,
				  (font.width / FONT_TILES_X) * ROWS_X,
				  (font.height / FONT_TILES_Y) * ROWS_Y);
		glViewport(0,
			   0,
			   (font.width / FONT_TILES_X) * ROWS_X,
			   (font.height / FONT_TILES_Y) * ROWS_Y);
		Logger::debug("window", "Initializing FontManager...");

		EventBus::init();
		FontManager::init();
		EntityHandler::init();
		ItemHandler::init();

		EntityPlayerInstanceRef player =
		  EntityHandler::createPlayer(playerColor, Alignment{});

		EntityInstanceRef demona =
		  EntityHandler::createEntity(getLocaleToken("ent_water_demon"));
		demona->setPosition(1, 1);
		EntityInstanceRef demoni =
		  EntityHandler::createEntity(getLocaleToken("ent_fire_demon"));
		demoni->setPosition(1, 2);
		EntityInstanceRef demont =
		  EntityHandler::createEntity(getLocaleToken("ent_earth_demon"));
		demont->setPosition(1, 3);
		EntityInstanceRef demonc =
		  EntityHandler::createEntity(getLocaleToken("ent_air_demon"));
		demonc->setPosition(1, 4);
	}

	void Window::loop() {
		if (destroyed) {
			Logger::fatal("window",
				      "Something went wrong, and the game "
				      "couldn't be started");
			return;
		}

		SDL_Event evt;
		bool quitRequested = false;
		auto clockpos = SDL_GetTicks();
		while (!quitRequested) {
			if (SDL_PollEvent(&evt)) {
				switch (evt.type) {
					case SDL_QUIT: {
						quitRequested = true;
						break;
					}
					case SDL_KEYUP: {
						if (evt.key.keysym.sym ==
						    SDLK_ESCAPE) {
							quitRequested = true;
							break;
						}
					}
				}

				if (callbacks.find(evt.type) !=
				    callbacks.end()) {
					for (auto f : callbacks.at(evt.type)) {
						f(&evt);
					}
				}
			}

			draw();

			auto delta = SDL_GetTicks() - clockpos;
			clockpos = SDL_GetTicks();

			if (delta != 0) {
				lastFps += 1000 / delta;
				fpsCounter++;
				fpsTimer += delta;

				if (fpsTimer >= 1000) {
					avgFps = lastFps / fpsCounter;
					lastFps = 0;
					fpsCounter = 0;
					fpsTimer = 0;
				}
			}

			SDL_GL_SwapWindow(window);
		}
	}

	void Window::draw() {
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		FontManager::clearScreen();

		FontManager::renderBorder(0, 7, 0);

		FontManager::renderText("FPS: ", 3, 0, 3, 3, 1);
		FontManager::renderInt(avgFps, 8, 0, 3, 3, 1);

		ItemHandler::draw();
		EntityHandler::draw();

		ItemHandler::update();
		EntityHandler::update();

		auto evt = EventBus::pollBus("WORLD");
		if (evt != nullptr) {
			if (evt->getType() == EventType::WORLD_TICK) {
				EntityHandler::tick();
			}
		}

		FontManager::buildScreen(vertColours, texCoords);

		glBindBuffer(GL_ARRAY_BUFFER, 2);
		glBufferData(GL_ARRAY_BUFFER,
			     sizeof(vertColours),
			     vertColours,
			     GL_STREAM_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 3);
		glBufferData(GL_ARRAY_BUFFER,
			     sizeof(texCoords),
			     texCoords,
			     GL_STATIC_DRAW);

		glDrawArrays(GL_TRIANGLES, 0, ROWS_X * ROWS_Y * VERTS_PER_TILE);
	}

	void Window::destroy() {
		EntityHandler::destroy();
		SDL_GL_DeleteContext(context);
		SDL_DestroyWindow(window);
		window = nullptr;
		SDL_Quit();
		destroyed = true;
	}

	void Window::bindCallback(int type,
				  std::function<void(SDL_Event*)> func) {
		if (callbacks.find(type) == callbacks.end()) {
			callbacks.insert(
			  { type,
			    std::vector<std::function<void(SDL_Event*)>>{} });
		}

		callbacks.at(type).push_back(func);
	}
} // namespace mlnm
