#!/bin/sh

if ([ $# -gt 1 ] || [ $# -eq 1 ]) && [ "$1" = "--clean" ]
then
	make clean
fi

make && cd bin && ./millennium
cd ..